import hashlib, constants, requests, json
from pprint import pprint


def get_sign(request, keys_required):
    '''get sign from request data:
    sign : 3b46dbe0a5348ae2bc516f814940efe2
    '''
    string_to_sign = ":".join(str(request[k]) for k in keys_required) + constants.secret

    sign = hashlib.md5(string_to_sign.encode()).hexdigest()
    print(sign)
    return sign


def send_postAPI(id, sign, req):
    '''Get confirmation response from API in format:
      {'data': {'data': {'lang': 'ru',
                   'm_curorderid': '67610022',
                   'm_historyid': '548670166',
                   'm_historytm': '1524067929',
                   'referer': 'https://payeer.com/merchant/?m_historyid=548670166&m_historytm=1524067929&m_curorderid=67610022&lang=ru'},
          'invoice_id': 33900454,
          'method': 'GET',
          'source': 'https://payeer.com/api/merchant/process.php'},
 'message': 'Ok',
 'result': 'ok'}'''

    url = 'https://central.pay-trio.com/invoice'
    request = {"shop_id": constants.shop_id, "amount": req["summ"], "payway": "payeer_eur", "currency": 978,
               "shop_invoice_id": id, "description": req["txt"], "sign": sign}
    headers = {'content-type': "application/json"}
    r = requests.post(url, data=json.dumps(request), headers=headers, verify=False)
    pprint(r.json())
    return json.loads(r.text)


def getTIPsign(id, req):
    '''Get sign according to the Form information and required_keys
    ("amount", "currency", "shop_id", "shop_invoice_id"):
        returns - 6d46ddf0a3458ae2bc516f814940efdu5'''
    print(req)

    print(req)
    request = {"shop_id": constants.shop_id, "amount": req["summ"], "payway": "card_uah", "currency": 840,
               "shop_invoice_id": id, "description": req["txt"]}
    keys_sorted = ("amount", "currency", "shop_id", "shop_invoice_id")
    return get_sign(request, keys_sorted)


def getAPIsign(id, req):
    '''Get sign according to the Form information and required_keys
    ('amount', 'currency', 'payway', 'shop_id', 'shop_invoice_id'):
        returns - 3b46dbe0a5348ae2bc516f814940efe2'''
    print(req)
    request = {"shop_id": constants.shop_id, "amount": req["summ"], "payway": "payeer_eur", "currency": 978,
               "shop_invoice_id": id, "description": req["txt"]}
    keys_sorted = ('amount', 'currency', 'payway', 'shop_id', 'shop_invoice_id')
    return get_sign(request, keys_sorted)

import datetime
import os

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app import app, db

# app.config.from_object(os.environ['APP_SETTINGS'])
app.config.from_object(os.environ.get('APP_SETTINGS'))

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)


class Logs(db.Model):
    __tablename__ = 'tb_pay_log'

    id = db.Column(db.Integer, primary_key=True)
    sum = db.Column(db.Float())
    ccy = db.Column(db.String(3))
    descr = db.Column(db.Text)
    status = db.Column(db.String(10))
    created_at = db.Column(db.DateTime(timezone=True), nullable=False, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(timezone=True), nullable=True, default=datetime.datetime.utcnow)


if __name__ == '__main__':
    manager.run()

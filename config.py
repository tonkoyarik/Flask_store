import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'i1LopvdDnHionkXbFV676X43xIMMKDGLK'
    POSTGRES = {
        'user': 'ukdvjevdnkuhmo',
        'pw': '5e95755d260dd919ce117e2f89ab56ae1986298923a7e3076e7bc8be763428d7',
        'db': 'd1pemljrc5q47i',
        'host': 'ec2-23-21-121-220.compute-1.amazonaws.com',
        'port': '5432',
    }
    SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES
    DATABASE_URL = os.environ['DATABASE_URL']


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True

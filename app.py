import os
from flask import render_template, Flask, request, send_from_directory, send_file
from flask_sqlalchemy import SQLAlchemy
import psycopg2
import pay_API, constants, log_db
from flask_heroku import Heroku

app = Flask(__name__, static_url_path="", static_folder='static')
'''
To run the code on the local machine, please use the following DB configs
POSTGRES = {
    'user': 'blog_user',
    'pw': '111111',
    'db': 'store_2',
    'host': 'localhost',
    'port': '5432',
}'''

POSTGRES = {
    'user': 'ukdvjevdnkuhmo',
    'pw': '5e95755d260dd919ce117e2f89ab56ae1986298923a7e3076e7bc8be763428d7',
    'db': 'd1pemljrc5q47i',
    'host': 'ec2-23-21-121-220.compute-1.amazonaws.com',
    'port': '5432',
}

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES

# DATABASE_URL = os.environ['DATABASE_URL']
# conn = psycopg2.connect(DATABASE_URL, sslmode='require')

# app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
heroku = Heroku(app)
db = SQLAlchemy(app)


@app.route('/', methods=['GET', 'POST'])
def router():
    '''View checks the currency (USD or EUR) from Form
     and then transfer the customer to the required
     page with conformation information
     '''

    if request.method == 'POST':

        id = log_db.add_pay(request.form)

        if request.form["sel"] == "840":
            ''''''
            sign = pay_API.getTIPsign(id, request.form)

            return render_template('payTIP.html', data=request.form, id=id, sign=sign)

        if request.form["sel"] == "978":
            sign = pay_API.getAPIsign(id, request.form)

            text_r = pay_API.send_postAPI(id, sign, request.form)
            print(text_r)

            return render_template('payAPI.html', data=request.form, id=id, sign=sign, text_r=text_r)

    else:

        return render_template('hello.html', data=constants.pay_cur)


@app.route('/log', methods=['GET', 'POST'])
def log():
    '''View returns the list last 100 payments '''

    if request.method == 'GET':
        req = log_db.get_last()
        return render_template('log.html', data=req)
    else:
        return render_template('hello.html', data=constants.pay_cur)


@app.route('/success')
def success():
    return "<h1>success :)</h1>"


@app.route('/failed')
def failed():
    return "<h1>failed</h1>"


@app.route('/mess')
def mess():
    print(request, request.form)
    return "<h1>message - OK </h1>"


if __name__ == '__main__':
    app.run(host='localhost', port=8000)

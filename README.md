##Flask app

This project can be implemented for any inline store to process payments with pay-trio (https://pay-trio.com/) platform
You can adapt it for your own website

#### Installation

`pip install -r requirements.txt`

## to run:

`python manage.py runserver`
## Additional Information

App has been already deployed on Heroku and you can access it with the following link: 

`https://stark-anchorage-64619.herokuapp.com/`

To run this code on your own machine with 
own DB, please change DB settings in app.py and then migrate DB:

`python manage.py db init`
`python manage.py db migrate`





`